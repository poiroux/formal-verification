import stainless.lang._
import stainless.collection._
import stainless.util.Random
import java.io.Reader
import Tromp.*

object HighLevelExecution {
  abstract sealed class Event
  case class BeginWrite()             extends Event // whenever the writer line number changes to 0
  case class EndWrite(value: Boolean) extends Event // whenever the writer line number changes to -1
  case class BeginRead()              extends Event // whenever the reader line number changes to 0
  case class EndRead(value: Boolean)  extends Event // whenever the reader line number changes to -1

  def isReadEvent(e: Event): Boolean = {
    e match
      case BeginRead() => true
      case EndRead(_)  => true
      case _           => false
  }

  def isWriteEvent(e: Event): Boolean = {
    e match
      case BeginWrite() => true
      case EndWrite(_)  => true
      case _            => false
  }

  def isExecutionAuxiliary(t: List[Event], expectingBeginRead: Boolean, expectingBeginWrite: Boolean): Boolean = {
    decreases(t)
    t match
      case Nil()                                           => !(expectingBeginRead || expectingBeginWrite)
      case Cons(BeginWrite(), tail) if expectingBeginWrite => isExecutionAuxiliary(tail, expectingBeginRead, false)
      case Cons(EndWrite(_), tail) if !expectingBeginWrite => isExecutionAuxiliary(tail, expectingBeginRead, true)
      case Cons(BeginRead(), tail) if expectingBeginRead   => isExecutionAuxiliary(tail, false, expectingBeginWrite)
      case Cons(EndRead(_), tail) if !expectingBeginRead   => isExecutionAuxiliary(tail, true, expectingBeginWrite)
      case _                                               => false
  }

  def isExecution(t: List[Event]): Boolean = {
    isExecutionAuxiliary(t, false, false) ||
    isExecutionAuxiliary(t, true, true) ||
    isExecutionAuxiliary(t, true, false) ||
    isExecutionAuxiliary(t, false, true)
  }

  def gotoLastReadEvent(t: List[Event]): List[Event] = {
    require(isExecution(t))
    decreases(t)
    t match
      case Nil()                   => t
      case Cons(EndRead(_), tail)  => t
      case Cons(BeginRead(), tail) => t
      case Cons(_, tail)           => gotoLastReadEvent(tail)
  }.ensuring(res =>
    // res is a tail of t
    val isTail = t == t.take(t.length - res.length) ++ res
    // the prefix does not contain any read event
    val noReadInPrefix = t.take(t.length - res.length).forall {
      case BeginRead() => false
      case EndRead(_)  => false
      case _           => true
    }
    // the tail starts with a read event or is empty
    val tailStartsWithRead = res match {
      case Nil()                => true
      case Cons(BeginRead(), _) => true
      case Cons(EndRead(_), _)  => true
      case _                    => false
    }
    isTail && noReadInPrefix && tailStartsWithRead && isExecution(res)
  )

  def gotoLastWriteEvent(t: List[Event]): List[Event] = {
    require(isExecution(t))
    decreases(t)
    t match
      case Nil()                    => t
      case Cons(EndWrite(_), tail)  => t
      case Cons(BeginWrite(), tail) => t
      case Cons(_, tail)            => gotoLastWriteEvent(tail)
  }.ensuring(res =>
    // res is a tail of t
    val isTail = t == t.take(t.length - res.length) ++ res
    // the prefix does not contain any write event
    val noWriteInPrefix = t.take(t.length - res.length).forall {
      case BeginWrite() => false
      case EndWrite(_)  => false
      case _            => true
    }
    // the tail starts with a write event or is empty
    val tailStartsWithWrite = res match {
      case Nil()                 => true
      case Cons(BeginWrite(), _) => true
      case Cons(EndWrite(_), _)  => true
      case _                     => false
    }
    isTail && noWriteInPrefix && tailStartsWithWrite && isExecution(res)
  )

  def readOnGoing(t: List[Event]): Boolean = {
    require(isExecution(t))
    gotoLastReadEvent(t) match
      case Cons(BeginRead(), _) => true
      case _                    => false
  }

  def writeOnGoing(t: List[Event]): Boolean = {
    require(isExecution(t))
    gotoLastWriteEvent(t) match
      case Cons(BeginWrite(), _) => true
      case _                     => false
  }

  def expectingBeginReadIsPropagated(t: List[Event]): Unit = {
    require(isExecutionAuxiliary(t, true, false) || isExecutionAuxiliary(t, true, true))
  }.ensuring(_ =>
    val subR = gotoLastReadEvent(t)
    t.length == subR.length || isExecutionAuxiliary(t.tail, true, false) || isExecutionAuxiliary(t.tail, true, true)
  )

  def expectingBeginWriteIsPropagated(t: List[Event]): Unit = {
    require(isExecutionAuxiliary(t, false, true) || isExecutionAuxiliary(t, true, true))
  }.ensuring(_ =>
    val subW = gotoLastWriteEvent(t)
    t.length == subW.length || isExecutionAuxiliary(t.tail, false, true) || isExecutionAuxiliary(t.tail, true, true)
  )

  def closedExecutionReadLemma(t: List[Event]): Unit = {
    require(isExecutionAuxiliary(t, true, false) || isExecutionAuxiliary(t, true, true))

    expectingBeginReadIsPropagated(t)
    val subR = gotoLastReadEvent(t)
    if t.length != subR.length then {
      closedExecutionReadLemma(t.tail)
    }
  }.ensuring(_ =>
    val subR = gotoLastReadEvent(t)
    isExecutionAuxiliary(subR, true, false) || isExecutionAuxiliary(subR, true, true)
  )

  def closedExecutionWriteLemma(t: List[Event]): Unit = {
    require(isExecutionAuxiliary(t, false, true) || isExecutionAuxiliary(t, true, true))

    expectingBeginWriteIsPropagated(t)
    val subW = gotoLastWriteEvent(t)
    if t.length != subW.length then {
      closedExecutionWriteLemma(t.tail)
    }
  }.ensuring(_ =>
    val subW = gotoLastWriteEvent(t)
    isExecutionAuxiliary(subW, false, true) || isExecutionAuxiliary(subW, true, true)
  )

  def isClosedExecutionAux(t: List[Event]): Unit = {
    require(isExecution(t))
    require(!readOnGoing(t))
    require(!writeOnGoing(t))

    assert(!isExecutionAuxiliary(t, true, true))
    if isExecutionAuxiliary(t, true, false) then closedExecutionReadLemma(t)
    if isExecutionAuxiliary(t, false, true) then closedExecutionWriteLemma(t)
  }.ensuring(_ => isExecutionAuxiliary(t, false, false))

  def isClosedExecution(t: List[Event]): Boolean = {
    isExecution(t) && !readOnGoing(t) && !writeOnGoing(t)
  }.ensuring(res =>
    if res then {
      isClosedExecutionAux(t)
      isExecutionAuxiliary(t, false, false)
    } else true
  )

  // BeginWrite and EndWrite event are alternating in an execution
  def writeAlternating(t: List[Event]): Unit = {
    require(isExecution(t))
  }.ensuring(_ =>
    t match
      case Cons(BeginWrite(), tail) =>
        gotoLastWriteEvent(tail) match
          case Nil()                => true
          case Cons(EndWrite(_), _) => true
          case _                    => false
      case Cons(EndWrite(_), tail) =>
        gotoLastWriteEvent(tail) match
          case Cons(BeginWrite(), _) => true
          case _                     => false
      case _ => true
  )

  // BeginRead and EndRead event are alternating in an execution
  def readAlternating(t: List[Event]): Unit = {
    require(isExecution(t))
  }.ensuring(_ =>
    t match
      case Cons(BeginRead(), tail) =>
        gotoLastReadEvent(tail) match
          case Nil()               => true
          case Cons(EndRead(_), _) => true
          case _                   => false
      case Cons(EndRead(_), tail) =>
        gotoLastReadEvent(tail) match
          case Cons(BeginRead(), _) => true
          case _                    => false
      case _ => true
  )

  def getLastClosedExecution(t: List[Event]): List[Event] = {
    require(isExecution(t))
    decreases(t)
    if isClosedExecution(t) then t
    else getLastClosedExecution(t.tail)
  }.ensuring(res => isClosedExecution(res))

  def closeExecution(t: List[Event]): List[Event] = {
    require(isExecution(t))
    decreases(t)
    if isClosedExecution(t) then t
    else {
      if writeOnGoing(t) then closeExecution(gotoLastWriteEvent(t).tail)
      else { // a read is on going, we remove it
        val subR = gotoLastReadEvent(t)
        t.take(t.length - subR.length) ++ subR.tail
      }
    }
  }.ensuring(res => isClosedExecution(res))

  def traceToExecution(t: List[(State, Action)]): List[Event] = {
    require(isTrompTrace(t))

    def traceToExecutionAux(t: List[(State, Action)]): List[Event] = {
      require(isTrompTrace(t))
      decreases(t)

      def expectingWriteEndOrNothing(t: List[(State, Action)]): Unit = {
        require(isTrompTrace(t))
        require(t.length >= 1)
        require(expectingWriteEndOrNothingAsPreviousWriteEndpoint(t.tail))
        decreases(t)
      }.ensuring(_ =>
        val rec = traceToExecutionAux(t.tail)
        isExecutionAuxiliary(rec, false, false) || isExecutionAuxiliary(rec, true, false)
      )

      def expectingWriteBegin(t: List[(State, Action)]): Unit = {
        require(isTrompTrace(t))
        require(t.length >= 1)
        require(expectingWriteStartAsPreviousWriteEndpoint(t.tail))
        decreases(t)
      }.ensuring(_ =>
        val rec = traceToExecutionAux(t.tail)
        isExecutionAuxiliary(rec, false, true) || isExecutionAuxiliary(rec, true, true)
      )

      def expectingReadEndOrNothing(t: List[(State, Action)]): Unit = {
        require(isTrompTrace(t))
        require(t.length >= 1)
        require(expectingReadEndOrNothingAsPreviousReadEndpoint(t.tail))
        decreases(t)
      }.ensuring(_ =>
        val rec = traceToExecutionAux(t.tail)
        isExecutionAuxiliary(rec, false, false) || isExecutionAuxiliary(rec, false, true)
      )

      def expectingReadBegin(t: List[(State, Action)]): Unit = {
        require(isTrompTrace(t))
        require(t.length >= 1)
        require(expectingReadStartAsPreviousReadEndpoint(t.tail))
        decreases(t)
      }.ensuring(_ =>
        val rec = traceToExecutionAux(t.tail)
        isExecutionAuxiliary(rec, true, false) || isExecutionAuxiliary(rec, true, true)
      )

      t match {
        case Cons((s, _), Cons((s_prev, a), _)) => {
          val rec = traceToExecutionAux(t.tail)

          a match {
            case WriterStep() => {
              if (isWriteStartTrompTrace(t)) {
                // it is sufficient to show assert(isExecutionAuxiliary(rec, false, false) || isExecutionAuxiliary(rec, true, false))
                expectingWriteEndOrNothing(t)
                Cons(BeginWrite(), rec)
              } else if (isWriteEndTrompTrace(t)) {
                // it is sufficient to show assert(isExecutionAuxiliary(rec, false, true) || isExecutionAuxiliary(rec, true, true))
                expectingWriteBegin(t)
                Cons(EndWrite(s.REG.value), rec)
              } else rec
            }
            case ReaderStep() => {
              if (isReadStartTrompTrace(t)) {
                // it is sufficient to show assert(isExecutionAuxiliary(rec, false, false) || isExecutionAuxiliary(rec, false, true))
                expectingReadEndOrNothing(t)
                Cons(BeginRead(), rec)
              } else if (isReadEndTrompTrace(t)) {
                // it is sufficient to show assert(isExecutionAuxiliary(rec, true, false) || isExecutionAuxiliary(rec, true, true))
                expectingReadBegin(t)
                Cons(EndRead(s.readerState.ret.get), rec)
              } else rec
            }
          }
        }
        case _ => Nil()
      }
    }.ensuring(isExecution(_)) // TODO: we need to get stronger invariants

    closeExecution(traceToExecutionAux(t))
  }.ensuring(isClosedExecution(_)) // TODO: we need stronger ensurings to help the linearize method

  def isClosedLinearExecutionAux(t: List[Event], lastRead: Option[Boolean]): Boolean = {
    decreases(t)
    t match
      case Nil()                                     => true
      case Cons(EndRead(v), Cons(BeginRead(), tail)) => isClosedLinearExecutionAux(tail, Some(v))
      case Cons(EndWrite(v), Cons(BeginWrite(), tail)) =>
        lastRead match
          case Some(v2) => v == v2 && isClosedLinearExecutionAux(tail, None())
          case None()   => isClosedLinearExecutionAux(tail, None())
      case _ => false
  }.ensuring(res => res ==> isClosedExecution(t))

  def isClosedLinearExecution(t: List[Event]): Boolean = {
    isClosedLinearExecutionAux(t, None())
  }.ensuring(res => res ==> isClosedExecution(t))

  def isLinearExecution(t: List[Event]): Boolean = {
    t match
      case Cons(BeginRead(), tail)  => isClosedLinearExecution(tail)
      case Cons(BeginWrite(), tail) => isClosedLinearExecution(tail)
      case _                        => isClosedLinearExecution(t)
  }.ensuring(res => res ==> isExecution(t))

  def closedAndLinearExecution(t: List[Event]): Unit = {
    require(isLinearExecution(t))
    require(isClosedExecution(t))
  }.ensuring(_ => isClosedLinearExecution(t))

  def linearize(t: List[(State, Action)]): List[Event] = {
    require(isTrompTrace(t))

    val hl = traceToExecution(t)
    assert(isClosedExecution(hl))

    val res = linearize_aux(hl, None(), None(), true, true)
    assert(isClosedLinearExecution(res))
    assert(isPermutation(hl, res))
    assert(equivalentPartialOrder(hl, res))
    assert(readsAreInSameOrder(hl, res))
    assert(writesAreInSameOrder(hl, res))
    assert(areEquivalent(hl, res))
    res
  }.ensuring(res => isLinearExecution(res) && areEquivalent(traceToExecution(t), res))

  def linearize_aux(
      hle: List[Event],
      last_read_event: Option[Event],
      last_write_event: Option[Event],
      lre_linearized: Boolean,
      lwe_linearized: Boolean
  ): List[Event] = {
    require(isExecution(hle))
    decreases(hle)

    // last read/write event is EndRead/EndWrite <==> a read/write is ongoing
    // this makes the method requires that the initial execution is closed
    require((last_read_event == Some(EndRead(true)) || last_read_event == Some(EndRead(false))) == readOnGoing(hle))
    require((last_write_event == Some(EndWrite(true)) || last_write_event == Some(EndWrite(false))) == writeOnGoing(hle))
    require(last_read_event.isEmpty || isReadEvent(last_read_event.get))
    require(last_write_event.isEmpty || isWriteEvent(last_write_event.get))

    // we need stronger hypotheses to prove the postconditions

    (hle, last_read_event, last_write_event) match {
      case (Cons(h, t), lre, lwe) => {
        h match {
          case EndWrite(value) if (lwe.isEmpty || lwe == Some(BeginWrite())) => {
            assert(!writeOnGoing(hle))
            writeAlternating(hle)
            assert(writeOnGoing(hle.tail))
            if lre == Some(EndRead(value)) && !lre_linearized then { // linearize the pending read
              val rec = linearize_aux(hle.tail, lre, Some(EndWrite(value)), true, false)
              Cons(EndRead(value), Cons(BeginRead(), rec))
            } else // delay linearization of the write
              linearize_aux(hle.tail, lre, Some(EndWrite(value)), lre_linearized, false)
          }
          case BeginWrite() if (lwe == Some(EndWrite(true)) || lwe == Some(EndWrite(false))) => {
            assert(writeOnGoing(hle))
            writeAlternating(hle)
            assert(!writeOnGoing(hle.tail))
            val rec = linearize_aux(hle.tail, lre, Some(BeginWrite()), lre_linearized, true)
            if lwe_linearized then rec                  // write already linearized, skip event
            else Cons(lwe.get, Cons(BeginWrite(), rec)) // write must be linearized
          }
          case EndRead(value) if (lre.isEmpty || lre == Some(BeginRead())) => {
            assert(!readOnGoing(hle))
            readAlternating(hle)
            assert(readOnGoing(hle.tail))
            if lwe == Some(EndWrite(!value)) && !lwe_linearized then { // linearize read, then write
              val rec = linearize_aux(hle.tail, Some(EndRead(value)), lwe, true, true)
              Cons(EndWrite(!value), Cons(BeginWrite(), Cons(EndRead(value), Cons(BeginRead(), rec))))
            } else // delay linearization
              linearize_aux(hle.tail, Some(EndRead(value)), lwe, false, lwe_linearized)
          }
          case BeginRead() if (lre == Some(EndRead(true)) || lre == Some(EndRead(false))) => {
            assert(readOnGoing(hle))
            readAlternating(hle)
            assert(!readOnGoing(hle.tail))
            val rec = linearize_aux(hle.tail, Some(BeginRead()), lwe, true, lwe_linearized)
            if lre_linearized then rec                 // read already linearized, skip event
            else Cons(lre.get, Cons(BeginRead(), rec)) // read must be linearized
          }
          case _ =>
            assert(false)
            List()
        }
      }
      case (Nil(), lre, lwe) => {
        assert(lre.isEmpty || lre == Some(BeginRead()))
        assert(lwe.isEmpty || lwe == Some(BeginWrite()))
        Nil()
      }
    }
    // }.ensuring(res => isClosedLinearExecution(res))
    // }.ensuring(res => isClosedLinearExecution(res) && equivalentPartialOrder(hle, res))
    // }.ensuring(res => isClosedLinearExecution(res) && (isClosedExecution(hle) ==> hle.length == res.length))
    // }.ensuring(res => isClosedLinearExecution(res) && (isClosedExecution(hle) ==> (readsAreInSameOrder(hle, res) && writesAreInSameOrder(hle, res))))
    // }.ensuring(res => isClosedLinearExecution(res) && (isClosedExecution(hle) ==> isPermutation(hle, res)))
  }.ensuring(res =>
    isClosedLinearExecution(res) && equivalentPartialOrder(hle, res) && (isClosedExecution(hle) ==> (isPermutation(hle, res) && readsAreInSameOrder(
      hle,
      res
    ) && writesAreInSameOrder(hle, res)))
  )

  // Sanity check
  def linearizeLinearExecution(t: List[(State, Action)]): Unit = {
    require(isTrompTrace(t))
    require(isLinearExecution(traceToExecution(t)))
  }.ensuring(_ => linearize(t) == traceToExecution(t))

  def isPermutation(t1: List[Event], t2: List[Event]): Boolean = {
    decreases(t1)
    if (t1.length != t2.length) then false
    else
      t1 match
        case Nil() => true
        case Cons(h, tail) =>
          val index = t2.indexOf(h)
          if index == -1 then false
          else isPermutation(tail, t2.take(index) ++ t2.drop(index + 1))
  }

  def equivalentPartialOrder(t1: List[Event], linear_t2: List[Event]): Boolean = {
    require(isExecution(t1))
    require(isLinearExecution(linear_t2))
    decreases(t1)

    if writeOnGoing(t1) || readOnGoing(t1) then {
      // Case 1: there are interleaved reads and writes in t1
      !linear_t2.isEmpty && equivalentPartialOrder(t1.tail, linear_t2.tail)
    } else {
      // Case 2: t1 is a closed execution
      linear_t2 match
        case Cons(BeginWrite(), _) => false
        case Cons(BeginRead(), _)  => false
        case _ =>
          t1 match
            case Nil() => true
            case Cons(EndWrite(v), Cons(BeginWrite(), tail)) =>
              linear_t2 match
                case Cons(EndWrite(v2), Cons(BeginWrite(), tail2)) => v == v2 && equivalentPartialOrder(tail, tail2)
                case _                                             => false
            case Cons(EndWrite(v), tail) if !linear_t2.isEmpty => equivalentPartialOrder(tail, linear_t2.tail)
            case Cons(EndRead(v), Cons(BeginRead(), tail)) =>
              linear_t2 match
                case Cons(EndRead(v2), Cons(BeginRead(), tail2)) => v == v2 && equivalentPartialOrder(tail, tail2)
                case _                                           => false
            case Cons(EndRead(v), tail) if !linear_t2.isEmpty => equivalentPartialOrder(tail, linear_t2.tail)
            case _                                            => false
    }
  }

  // This second version is maybe more handy, but it requires stronger hypothesis
  def equivalentPartialOrderWeak(t1: List[Event], linear_t2: List[Event]): Boolean = {
    require(isExecution(t1))
    require(isLinearExecution(linear_t2))
    require(t1.length == linear_t2.length)
    decreases(t1)

    if writeOnGoing(t1) || readOnGoing(t1) then {
      // Case 1: there are interleaved reads and writes in t1
      equivalentPartialOrderWeak(t1.tail, linear_t2.tail)
    } else {
      // Case 2: t1 is a closed execution
      linear_t2 match
        case Cons(BeginWrite(), _) => false
        case Cons(BeginRead(), _)  => false
        case _ =>
          t1 match
            case Nil() => true
            case Cons(EndWrite(v), Cons(BeginWrite(), tail)) =>
              linear_t2 match
                case Cons(EndWrite(v2), Cons(BeginWrite(), tail2)) => v == v2 && equivalentPartialOrderWeak(tail, tail2)
                case _                                             => false
            case Cons(EndWrite(v), tail) => equivalentPartialOrderWeak(tail, linear_t2.tail)
            case Cons(EndRead(v), Cons(BeginRead(), tail)) =>
              linear_t2 match
                case Cons(EndRead(v2), Cons(BeginRead(), tail2)) => v == v2 && equivalentPartialOrderWeak(tail, tail2)
                case _                                           => false
            case Cons(EndRead(v), tail) => equivalentPartialOrderWeak(tail, linear_t2.tail)
            case _                      => false
    }
  }

  def readsAreInSameOrder(t1: List[Event], t2: List[Event]): Boolean = {
    require(isExecution(t1))
    require(isExecution(t2))
    decreases(t1.length + t2.length)
    (gotoLastReadEvent(t1), gotoLastReadEvent(t2)) match
      case (Cons(EndRead(v1), _), Cons(EndRead(v2), _)) => v1 == v2 && readsAreInSameOrder(t1.tail, t2.tail)
      case (Cons(BeginRead(), _), Cons(BeginRead(), _)) => readsAreInSameOrder(t1.tail, t2.tail)
      case (Nil(), Nil())                               => true
      case _                                            => false
  }

  def writesAreInSameOrder(t1: List[Event], t2: List[Event]): Boolean = {
    require(isExecution(t1))
    require(isExecution(t2))
    decreases(t1.length + t2.length)
    (gotoLastWriteEvent(t1), gotoLastWriteEvent(t2)) match
      case (Cons(EndWrite(v1), _), Cons(EndWrite(v2), _)) => v1 == v2 && writesAreInSameOrder(t1.tail, t2.tail)
      case (Cons(BeginWrite(), _), Cons(BeginWrite(), _)) => writesAreInSameOrder(t1.tail, t2.tail)
      case (Nil(), Nil())                                 => true
      case _                                              => false
  }

  def areEquivalent(t1: List[Event], linear_t2: List[Event]): Boolean = {
    require(isExecution(t1))
    require(isLinearExecution(linear_t2))
    if (!isPermutation(t1, linear_t2)) then false
    else equivalentPartialOrder(t1, linear_t2) && readsAreInSameOrder(t1, linear_t2) && writesAreInSameOrder(t1, linear_t2)
  }

  /// Sanity checks
  // Let's prove that any execution is equivalent to itself
  def identityIsPermutation(t1: List[Event]): Unit = {
    decreases(t1)
    t1 match
      case Nil() => assert(isPermutation(t1, t1))
      case Cons(h, t) =>
        assert(0 == t1.indexOf(h))
        assert(t == t1.take(0) ++ t1.drop(1))
        identityIsPermutation(t)
  }.ensuring(_ => isPermutation(t1, t1))

  def selfEquivalentPartialOrder(t1: List[Event]): Unit = {
    require(isLinearExecution(t1))
    decreases(t1)
    if writeOnGoing(t1) || readOnGoing(t1) then selfEquivalentPartialOrder(t1.tail)
    else
      // Case 2: t1 is a closed execution
      t1 match
        case Nil()                                       => assert(equivalentPartialOrder(t1, t1))
        case Cons(EndWrite(v), Cons(BeginWrite(), tail)) => selfEquivalentPartialOrder(tail)
        case Cons(EndRead(v), Cons(BeginRead(), tail))   => selfEquivalentPartialOrder(tail)
        case Cons(EndWrite(v), tail)                     => selfEquivalentPartialOrder(tail)
        case Cons(EndRead(v), tail)                      => selfEquivalentPartialOrder(tail)
        case _                                           => assert(false)
  }.ensuring(_ => equivalentPartialOrder(t1, t1))

  def selfReadsAreInSameOrder(t1: List[Event]): Unit = {
    require(isExecution(t1))
    decreases(t1)
    t1 match
      case Nil()         => assert(readsAreInSameOrder(t1, t1))
      case Cons(_, tail) => selfReadsAreInSameOrder(tail)
  }.ensuring(_ => readsAreInSameOrder(t1, t1))

  def selfWritesAreInSameOrder(t1: List[Event]): Unit = {
    require(isExecution(t1))
    decreases(t1)
    t1 match
      case Nil()         => assert(writesAreInSameOrder(t1, t1))
      case Cons(_, tail) => selfWritesAreInSameOrder(tail)
  }.ensuring(_ => writesAreInSameOrder(t1, t1))

  def selfEquivalent(t1: List[Event]): Unit = {
    require(isLinearExecution(t1))
    identityIsPermutation(t1)
    selfEquivalentPartialOrder(t1)
    selfReadsAreInSameOrder(t1)
    selfWritesAreInSameOrder(t1)
  }.ensuring(_ => areEquivalent(t1, t1))

  /// MAIN THEOREM TO PROVE
  def isLinearizable(t: List[(State, Action)]): Unit = {
    require(isTrompTrace(t))
  }.ensuring(_ => areEquivalent(traceToExecution(t), linearize(t)))

  // Old alternative way with pi stuff

  // def atomicityProp1(t: List[(State, Action)]): Boolean = {
  //   require(isTrompTrace(t))
  //   val exec = traceToExecution(t)
  //   // 1. r does not precede π(r), i.e., ∀r, τe(r) ≥ τb(π(r))
  //   false
  // }

  // def atomicityProp2(t: List[(State, Action)]): Boolean = {
  //   require(isTrompTrace(t))
  //   val exec = traceToExecution(t)
  //   // 2. No read returns an overwritten value, i.e., ∀w : τe(w) ≤ τb(r) ⇒ (w = π(r) ∨ τe(w) ≤ τb(π(r)))
  //   false
  // }

  // def atomicityProp3(t: List[(State, Action)]): Boolean = {
  //   require(isTrompTrace(t))
  //   val exec = traceToExecution(t)
  //   // o new/old inversions, i.e., ∀r1, r2 : τe(r1) < τb(r2) ⇒ (π(r1) = π(r2) ∨ τe(π(r1)) ≤ τb(π(r2)))
  //   false
  // }
}
