import stainless.lang._
import stainless.collection._
import stainless.util.Random
import java.io.Reader

case class SafeRegister(
    value: Boolean = false,
    beingWritten: Boolean = false
) {
  def write_step: SafeRegister = {
    if (beingWritten) SafeRegister(!value, !beingWritten)
    else SafeRegister(value, !beingWritten)
  }

  def read_step: SafeRegister = {
    SafeRegister(value, beingWritten)
    // if (beingWritten) SafeRegister(stainless.util.Random(0).nextBoolean(), beingWritten)
    // else SafeRegister(value, beingWritten)
  }

}

object Tromp {
  abstract sealed class Action
  case class WriterStep() extends Action
  case class ReaderStep() extends Action

  case class WriterState(
      lineNumber: Int = -1,
      rr: Boolean = false
  ) {
    require(lineNumber >= -1 && lineNumber <= 4)

    def return_state: WriterState = WriterState()
  }

  case class ReaderState(
      lineNumber: Int = -1,
      value: Boolean = false,
      aux: Boolean = false,
      wr: Boolean = false,
      ret: Option[Boolean] = Some(false)
  ) {
    require(lineNumber >= -1 && lineNumber <= 10)

    def return_state(return_value: Boolean): ReaderState = {
      ReaderState(value = this.value, ret = Some(return_value), lineNumber = -1)
    }
  }

  case class State(
      writerState: WriterState = WriterState(),
      readerState: ReaderState = ReaderState(),
      REG: SafeRegister = SafeRegister(),
      WR: SafeRegister = SafeRegister(),
      RR: SafeRegister = SafeRegister()
  ) {}

  def writerStep(s: State): List[State] = {
    require(s.writerState.lineNumber >= -1 && s.writerState.lineNumber <= 4)
    s.writerState.lineNumber match {
      case -1 => { // Return (go to initialization)
        List(s.copy(writerState = WriterState(lineNumber = 0), readerState = s.readerState.copy(ret = None())))
      }
      case 0 => { // Begin writing REG <- !REG
        List(s.copy(writerState = s.writerState.copy(lineNumber = 1), REG = s.REG.write_step, readerState = s.readerState.copy(ret = None())))
      }
      case 1 => { // Finish writing REG <- !REG
        List(s.copy(writerState = s.writerState.copy(lineNumber = 2), REG = s.REG.write_step, readerState = s.readerState.copy(ret = None())))
      }
      case 2 => { // Read rr <- RR
        val read_s = s.RR.read_step
        if (s.RR.beingWritten) {
          List(
            s.copy(writerState = s.writerState.copy(rr = false, lineNumber = 3), RR = read_s, readerState = s.readerState.copy(ret = None())),
            s.copy(writerState = s.writerState.copy(rr = true, lineNumber = 3), RR = read_s, readerState = s.readerState.copy(ret = None()))
          )
        } else {
          List(
            s.copy(writerState = s.writerState.copy(rr = read_s.value, lineNumber = 3), RR = read_s, readerState = s.readerState.copy(ret = None()))
          )
        }
      }
      case 3 => { // If rr == WR
        if (s.writerState.rr == s.WR.value) { // Begin Write
          List(s.copy(writerState = s.writerState.copy(lineNumber = 4), WR = s.WR.write_step))
        } else { // Else, skip to return
          List(s.copy(writerState = s.writerState.copy(lineNumber = -1)))
        }
      }
      case 4 => { // End Write
        List(s.copy(writerState = s.writerState.copy(lineNumber = -1), WR = s.WR.write_step))
      }
      case _ => {
        List()
      }
    }
  }.ensuring(res => !res.isEmpty && res.forall(s => s.writerState.lineNumber >= -1 && s.writerState.lineNumber <= 4))

  def readerStep(s: State): List[State] = {
    require(s.readerState.lineNumber >= -1 && s.readerState.lineNumber <= 10)
    s.readerState.lineNumber match {
      case -1 => { // Universal return case
        List(s.copy(readerState = ReaderState(value = s.readerState.value, lineNumber = 0, ret = None())))
      }
      case 0 => { // wr <- WR
        val WR = s.WR.read_step
        if (s.WR.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(wr = false, lineNumber = 1), WR = WR),
            s.copy(readerState = s.readerState.copy(wr = true, lineNumber = 1), WR = WR)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(lineNumber = 1, wr = WR.value), WR = WR))
        }
      }
      case 1 => { // if wr == RR
        if (s.readerState.wr == s.RR.value) { // Skip to end
          List(s.copy(readerState = s.readerState.return_state(return_value = s.readerState.value)))
        } else { // Next line
          List(s.copy(readerState = s.readerState.copy(lineNumber = 2)))
        }
      }
      case 2 => { // aux <- REG
        val reg = s.REG.read_step
        if (s.REG.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(aux = false, lineNumber = 3), REG = reg),
            s.copy(readerState = s.readerState.copy(aux = true, lineNumber = 3), REG = reg)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(lineNumber = 3, aux = reg.value), REG = reg))
        }
      }
      case 3 => { // wr <- REG
        val WR = s.WR.read_step
        if (s.WR.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(wr = false, lineNumber = 4), WR = WR),
            s.copy(readerState = s.readerState.copy(wr = true, lineNumber = 4), WR = WR)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(lineNumber = 4, wr = WR.value), WR = WR))
        }
      }
      case 4 => { // if wr != RR
        if (s.readerState.wr != s.RR.value) { // Begin write
          val RR = s.RR.write_step
          List(s.copy(readerState = s.readerState.copy(lineNumber = 5), RR = RR))
        } else { // Next line
          List(s.copy(readerState = s.readerState.copy(lineNumber = 6)))
        }
      }
      case 5 => { // Finish writing
        val RR = s.RR.write_step
        List(s.copy(readerState = s.readerState.copy(lineNumber = 6), RR = RR))
      }
      case 6 => { // val <- REG
        val REG = s.REG.read_step
        if (s.REG.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(value = false, lineNumber = 7), REG = REG),
            s.copy(readerState = s.readerState.copy(value = true, lineNumber = 7), REG = REG)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(lineNumber = 7, value = REG.value), REG = REG))
        }
      }
      case 7 => { // wr <- REG
        val WR = s.WR.read_step
        if (s.WR.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(wr = false, lineNumber = 8), WR = WR),
            s.copy(readerState = s.readerState.copy(wr = true, lineNumber = 8), WR = WR)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(lineNumber = 8, wr = WR.value), WR = WR))
        }
      }
      case 8 => { // if wr == RR
        if (s.readerState.wr == s.RR.value) { // Skip to end
          List(s.copy(readerState = s.readerState.return_state(return_value = s.readerState.value)))
        } else List(s.copy(readerState = s.readerState.copy(lineNumber = 9)))
      }
      case 9 => { // val <- REG
        val REG = s.REG.read_step
        if (s.REG.beingWritten) {
          List(
            s.copy(readerState = s.readerState.copy(value = false, lineNumber = 10), REG = REG),
            s.copy(readerState = s.readerState.copy(value = true, lineNumber = 10), REG = REG)
          )
        } else {
          List(s.copy(readerState = s.readerState.copy(value = REG.value, lineNumber = 10)))
        }
      }
      case 10 => {
        List(s.copy(readerState = s.readerState.return_state(return_value = s.readerState.aux)))
      }
      case _ => {
        List()
      }
    }
  }.ensuring(res => !res.isEmpty && res.forall(s => s.readerState.lineNumber >= -1 && s.readerState.lineNumber <= 10))

  def isInitial(s: State): Boolean = {
    s.writerState == WriterState() &&
    s.readerState == ReaderState() &&
    s.REG == SafeRegister() &&
    s.WR == SafeRegister() &&
    s.RR == SafeRegister()
  }

  // By definition
  def isWriting(current: State): Boolean = {
    current.writerState.lineNumber != -1
  }

  // By definition
  def isReading(current: State): Boolean = {
    current.readerState.lineNumber != -1
  }

  // The start of a write happens, by definition,
  // when we switch from not writing to writing in two consecutive states
  def isWriteStart(current: State, previous: State): Boolean = {
    !isWriting(previous) && isWriting(current)
  }

  // The end of a write happens, by definition,
  // when we switch from writing to not writing in two consecutive states
  def isWriteEnd(current: State, previous: State): Boolean = {
    isWriting(previous) && !isWriting(current)
  }

  // The start of a read happens, by definition,
  // when we switch from not reading to reading in two consecutive states
  def isReadStart(current: State, previous: State): Boolean = {
    !isReading(previous) && isReading(current)
  }

  // The start of a read happens, by definition,
  // when we switch from not reading to reading in two consecutive states
  def isReadEnd(current: State, previous: State): Boolean = {
    isReading(previous) && !isReading(current)
  }

  def r(s1: State, a: Action): List[State] = {
    a match
      case ReaderStep() => readerStep(s1)
      case WriterStep() => writerStep(s1)
  }.ensuring(res => 1 <= res.length && res.length <= 2)

  def isTrompTraceLike(t: List[(State, Action)]): Boolean = {
    decreases(t)
    t match
      case Cons((s1, a1), Cons((s2, a2), rest)) =>
        if (r(s2, a2).contains(s1)) isTrompTraceLike((s2, a2) :: rest)
        else false
      case _ => true
  }

  def isTrompTrace(t: List[(State, Action)]): Boolean = { // last action is ignored
    t match
      case Cons(_, _) => isTrompTraceLike(t) && isInitial(t.last._1)
      case _          => true
  }

  def exampleTrace: List[(State, Action)] = {
    val s0 = State()
    val s1 = r(s0, WriterStep()).head
    val s2 = r(s1, WriterStep()).head
    val s3 = r(s2, WriterStep()).head
    val s4 = r(s3, WriterStep()).head
    List[(State, Action)](
      (s4, WriterStep()),
      (s3, WriterStep()),
      (s2, WriterStep()),
      (s1, WriterStep()),
      (s0, WriterStep())
    )
  }.ensuring(res => isTrompTrace(res))

  def lastActionIsIrrelevant(t: List[(State, Action)]): (List[(State, Action)], List[(State, Action)]) = {
    require(isTrompTrace(t))
    t match
      case Cons((s, a), rest) =>
        val t1 = Cons((s, ReaderStep()), rest)
        val t2 = Cons((s, WriterStep()), rest)
        (t1, t2)
      case Nil() => (Nil(), Nil())
  }.ensuring(res => isTrompTrace(res._1) && isTrompTrace(res._2))

  def subTracesAreTraces(t: List[(State, Action)]): Unit = {
    require(isTrompTrace(t))
    require(t.length > 1)
  }.ensuring(_ => isTrompTrace(t.tail))

  def isReadReturn(state: State): Boolean =
    state.readerState.ret match
      case Some(b) => true
      case None()  => false

  def isSerialRead(trace: List[(State, Action)]): Boolean = {
    require(isTrompTrace(trace))
    require(trace.isEmpty || isReadReturn(trace.head._1))

    def isSerialReadAux(trace: List[(State, Action)]): Boolean = {
      trace match
        case Nil() => true
        case Cons((s, ReaderStep()), rest) => {
          if s.writerState.lineNumber != -1 then false
          else if isReadReturn(s) then true
          else isSerialReadAux(rest)
        }
        case _ => false
    }

    if trace.isEmpty then true
    else isSerialReadAux(trace.tail)
  }

  def writeLineNumberCycleWriteStep(trace: List[(State, Action)]): Unit = {
    require(isTrompTrace(trace))
    require(trace.length > 1)
    require(trace.tail.head._2 == WriterStep())
  }.ensuring(_ =>
    val prev_state       = trace.tail.head._1
    val prev_line_number = prev_state.writerState.lineNumber
    val last_line_number = trace.head._1.writerState.lineNumber
    (last_line_number == -1 && prev_line_number != -1) || prev_line_number < last_line_number
  )

  def writeLineNumberCycleReadStep(trace: List[(State, Action)]): Unit = {
    require(isTrompTrace(trace))
    require(trace.length > 1)
    require(trace.tail.head._2 == ReaderStep())
  }.ensuring(_ =>
    val prev_state       = trace.tail.head._1
    val prev_line_number = prev_state.writerState.lineNumber
    val last_line_number = trace.head._1.writerState.lineNumber
    prev_line_number == last_line_number
  )

  def traceChanges(trace: List[(State, Action)]): Unit = {
    require(isTrompTrace(trace))
    require(trace.length > 1)
  }.ensuring(_ =>
    val prevState = trace.tail.head._1
    val currState = trace.head._1
    prevState.readerState.lineNumber != currState.readerState.lineNumber || prevState.writerState.lineNumber != currState.writerState.lineNumber
  )

  def expectingReadStartAsPreviousReadEndpoint(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    decreases(t)
    t match
      case Cons((s, _), Cons((s_p, _), rest)) =>
        if isReadStart(s, s_p) then true
        else if isReadEnd(s, s_p) then false
        else expectingReadStartAsPreviousReadEndpoint(rest)
      case _ => false
  }

  def expectingReadEndOrNothingAsPreviousReadEndpoint(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    decreases(t)
    t match
      case Cons((s, _), Cons((s_p, _), rest)) =>
        if isReadStart(s, s_p) then false
        else if isReadEnd(s, s_p) then true
        else expectingReadEndOrNothingAsPreviousReadEndpoint(rest)
      case _ => true
  }

  def isReadStartTrompTrace(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    require(t.length >= 2)
    isReadStart(t.head._1, t.tail.head._1)
  }.ensuring(res => res ==> (t.head._1.readerState.ret.isEmpty && expectingReadEndOrNothingAsPreviousReadEndpoint(t.tail)))

  def isReadEndTrompTrace(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    require(t.length >= 2)
    isReadEnd(t.head._1, t.tail.head._1)
  }.ensuring(res => res ==> (!t.head._1.readerState.ret.isEmpty && expectingReadStartAsPreviousReadEndpoint(t.tail)))

  def expectingWriteStartAsPreviousWriteEndpoint(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    decreases(t)
    t match
      case Cons((s, _), Cons((s_p, _), rest)) =>
        if isWriteStart(s, s_p) then true
        else if isWriteEnd(s, s_p) then false
        else expectingWriteStartAsPreviousWriteEndpoint(rest)
      case _ => false
  }

  def expectingWriteEndOrNothingAsPreviousWriteEndpoint(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    decreases(t)
    t match
      case Cons((s, _), Cons((s_p, _), rest)) =>
        if isWriteStart(s, s_p) then false
        else if isWriteEnd(s, s_p) then true
        else expectingWriteEndOrNothingAsPreviousWriteEndpoint(rest)
      case _ => true
  }

  def isWriteStartTrompTrace(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    require(t.length >= 2)
    isWriteStart(t.head._1, t.tail.head._1)
  }.ensuring(res => res ==> expectingWriteEndOrNothingAsPreviousWriteEndpoint(t.tail))

  def isWriteEndTrompTrace(t: List[(State, Action)]): Boolean = {
    require(isTrompTrace(t))
    require(t.length >= 2)
    isWriteEnd(t.head._1, t.tail.head._1)
  }.ensuring(res => res ==> expectingWriteStartAsPreviousWriteEndpoint(t.tail))

  // def extractLastReadStart(t: List[(State, Action)]): List[(State, Action)] = {
  //   require(isTrompTrace(t))
  //   require(existReadStart(t))
  //   decreases(t)

  //   t match
  //     case Cons(_, Cons(_, rest)) =>
  //       if isReadStartTrompTrace(t) then t
  //       else extractLastReadStart(rest)
  //     case _ => List()
  // }.ensuring(res => isTrompTrace(res) && isReadStartTrompTrace(res))
}

object IntermediateLevelExecution {
  import Tromp._

  abstract sealed class IntermediateEvent
  case class BeginWrite(value: Boolean)    extends IntermediateEvent // whenever the writer line number changes to 0
  case class EndWrite(value: Boolean)      extends IntermediateEvent // whenever the writer line number changes to -1
  case class BeginRead()                   extends IntermediateEvent // whenever the reader line number changes to 0
  case class ReturnVal(value: Boolean)     extends IntermediateEvent // whenever the reader line number changes to -1 and it returns val
  case class ReturnAux(value: Boolean)     extends IntermediateEvent // whenever the reader line number changes to -1 and it returns aux
  case class SetVal(value: Boolean)        extends IntermediateEvent // whenever the reader sets val
  case class SetAux(value: Boolean)        extends IntermediateEvent // whenever the reader sets aux
  case class BeginWriteREG(value: Boolean) extends IntermediateEvent // whenever the writer begins writing REG
  case class EndWriteREG(value: Boolean)   extends IntermediateEvent // whenever the writer finishes writing REG
  case class OtherEvent()                  extends IntermediateEvent // anything else

  def isReturnVal(current: State, previous: State): Boolean = {
    isReadEnd(current, previous) &&
    (previous.readerState.lineNumber == 8 || previous.readerState.lineNumber == 1)
  }

  def isReturnAux(current: State, previous: State): Boolean = {
    isReadEnd(current, previous) && previous.readerState.lineNumber == 10
  }

  // When the reader's `value` variable is set
  def isSetVal(current: State, previous: State): Boolean = {
    (current.readerState.lineNumber == 7 && previous.readerState.lineNumber == 6) ||
    (current.readerState.lineNumber == 10 && previous.readerState.lineNumber == 9)
  }

  // When the reader's `aux` variable is set
  def isSetAux(current: State, previous: State): Boolean = {
    (current.readerState.lineNumber == 2 && previous.readerState.lineNumber == 3)
  }

  // When the writer starts writing REG
  def isRegWriteStart(current: State, previous: State): Boolean = {
    (current.writerState.lineNumber == 1 && previous.readerState.lineNumber == 0)
  }

  // When the writer finishes writing REG
  def isRegWriteEnd(current: State, previous: State): Boolean = {
    (current.writerState.lineNumber == 2 && previous.readerState.lineNumber == 1)
  }

  def isExtensionOf[T](l1: List[T], l2: List[T]): Boolean = {
    if l1.length > l2.length then {
      isExtensionOf(l1.tail, l2)
    } else {
      l1 == l2
    }
  }

  def traceMinusHeadIsTrace(t: List[(State, Action)]): Unit = {
    require(isTrompTrace(t))
    require(!t.isEmpty)
  }.ensuring(
    isTrompTrace(t.tail)
  )

  def nonEmptySubtraceIsTrace(t1: List[(State, Action)], t2: List[(State, Action)]): Unit = {
    require(isTrompTrace(t1))
    require(isExtensionOf(t1, t2))
    decreases(t1)

    assert(t1.length >= t2.length)
    if t1.length == t2.length then {
      assert(t1 == t2)
      assert(isTrompTrace(t2))
    } else if t2.length != 0 then {
      assert(t1.length > 1)
      traceMinusHeadIsTrace(t1)
      assert(isTrompTrace(t1.tail))
      assert(isExtensionOf(t1.tail, t2))
      nonEmptySubtraceIsTrace(t1.tail, t2)
    }
  }.ensuring(
    isTrompTrace(t2)
  )

  def lastStepRespectingCondition(t: List[(State, Action)], condition: ((State, Action), (State, Action)) => Boolean): List[(State, Action)] = {
    require(isTrompTrace(t) || t.isEmpty)
    decreases(t)

    t match {
      case Cons((current, a2), Cons((previous, a1), _)) => {
        if condition((current, a2), (previous, a1)) then t
        else lastStepRespectingCondition(t.tail, condition)
      }
      case otherwise => List()
    }
  }.ensuring(res => res.length <= t.length)

  def lastStepRespectingConditionIsGood(t: List[(State, Action)], condition: ((State, Action), (State, Action)) => Boolean): Unit = {
    require(isTrompTrace(t))
    decreases(t)

    if t.length > 1 then {
      lastStepRespectingConditionIsGood(t.tail, condition)
    }
  }.ensuring(_ =>
    val lsrc = lastStepRespectingCondition(t, condition)
    lsrc == Nil() || (lsrc.length > 1 && condition(lsrc.head, lsrc.tail.head))
  )

  def lastStepRespectingConditionIsSubtrace(t: List[(State, Action)], condition: ((State, Action), (State, Action)) => Boolean): Unit = {
    require(isTrompTrace(t))
    val lsrc = lastStepRespectingCondition(t, condition)
    require(!lsrc.isEmpty)
    decreases(t)

    if t == lsrc then {
      assert(isExtensionOf(t, lsrc))
      nonEmptySubtraceIsTrace(t, lsrc)
      assert(isTrompTrace(lsrc))
    } else {
      assert(lastStepRespectingCondition(t, condition) == lastStepRespectingCondition(t.tail, condition))

      lastStepRespectingConditionIsGood(t, condition)
      assert(lsrc.length > 1)
      assert(t.length >= lsrc.length)
      assert(isTrompTrace(t.tail))
      lastStepRespectingConditionIsSubtrace(t.tail, condition)

      assert(isExtensionOf(t, lsrc))
      nonEmptySubtraceIsTrace(t, lsrc)
      assert(isTrompTrace(lsrc))
    }
  }.ensuring(_ =>
    isExtensionOf(t, lastStepRespectingCondition(t, condition))
      && isTrompTrace(lastStepRespectingCondition(t, condition))
  )

  def isWriteStartAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isWriteStart(current._1, previous._1)
  }

  def isWriteEndAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isWriteEnd(current._1, previous._1)
  }

  def isReadStartAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadStart(current._1, previous._1)
  }

  def isReadEndAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadEnd(current._1, previous._1)
  }

  def isReadStep(current: (State, Action), previous: (State, Action)): Boolean = {
    previous._2 == ReaderStep()
  }

  def isWriteStep(current: (State, Action), previous: (State, Action)): Boolean = {
    previous._2 == WriterStep()
  }

  def isReturnVal(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadEnd(current._1, previous._1) &&
    (previous._1.readerState.lineNumber == 8 || previous._1.readerState.lineNumber == 1)
  }

  def isReturnAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadEnd(current._1, previous._1) && previous._1.readerState.lineNumber == 10
  }

  // When the reader's `value` variable is set
  def isSetVal(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadStep(current, previous) && (previous._1.readerState.lineNumber == 6 || previous._1.readerState.lineNumber == 9)
  }

  // When the reader's `aux` variable is set
  def isSetAux(current: (State, Action), previous: (State, Action)): Boolean = {
    isReadStep(current, previous) && previous._1.readerState.lineNumber == 2
  }

  // When the writer starts writing REG
  def isRegWriteStart(current: (State, Action), previous: (State, Action)): Boolean = {
    isWriteStep(current, previous) && previous._1.readerState.lineNumber == 0
  }

  // When the writer finishes writing REG
  def isRegWriteEnd(current: (State, Action), previous: (State, Action)): Boolean = {
    isWriteStep(current, previous) && previous._1.readerState.lineNumber == 1
  }

  def causingActionOfReturn(t: List[(State, Action)]): List[(State, Action)] = {
    require(isTrompTrace(t))
    require(t.length > 1)
    require(isReadEnd(t.head._1, t.tail.head._1)) // Is a return, i.e., ReturnVal or ReturnAux

    if isReturnVal(t.head, t.tail.head) then {
      lastStepRespectingConditionIsGood(t, isSetVal)
      val res = lastStepRespectingCondition(t, isSetVal)
      if !res.isEmpty then lastStepRespectingConditionIsSubtrace(t, isSetVal)
      res
    } else if isReturnAux(t.head, t.tail.head) then {
      lastStepRespectingConditionIsGood(t, isSetAux)
      val res = lastStepRespectingCondition(t, isSetAux)
      if !res.isEmpty then lastStepRespectingConditionIsSubtrace(t, isSetAux)
      res
    } else { List() } // unreachable
  }.ensuring(res => (isTrompTrace(res) && res.length != 1))

  def causingRegWriteOfAction(t: List[(State, Action)]): List[(State, Action)] = {
    require(isTrompTrace(t))
    require(t.length > 1)
    require(isSetAux(t.head, t.tail.head) || isSetVal(t.head, t.tail.head)) //  Is an action

    val value = if isSetAux(t.head, t.tail.head) then {
      t.head._1.readerState.aux
    } else { // isSetVal(t.head, t.tail.head)
      t.head._1.readerState.value
    }

    def isResponsibleRegWriteStart(current: (State, Action), previous: (State, Action)): Boolean = {
      isRegWriteStart(current, previous) && previous._1.WR.value != value
    }

    lastStepRespectingConditionIsGood(t, isResponsibleRegWriteStart)
    val res = lastStepRespectingCondition(t, isResponsibleRegWriteStart)
    if !res.isEmpty then lastStepRespectingConditionIsSubtrace(t, isResponsibleRegWriteStart)
    res
  }.ensuring(res => (isTrompTrace(res) && res.length != 1))

  def causingWriteOfRegWrite(t: List[(State, Action)]): List[(State, Action)] = {
    require(isTrompTrace(t))
    require(t.length > 1)
    require(isRegWriteStart(t.head, t.tail.head)) //  Is an action

    val res = lastStepRespectingCondition(t, isWriteStartAux)
    if !res.isEmpty then lastStepRespectingConditionIsSubtrace(t, isWriteStartAux)
    res
  }.ensuring(res => (isTrompTrace(res) && res.length != 1))

  def pi(t: List[(State, Action)]): List[(State, Action)] = {
    require(isTrompTrace(t))
    require(t.length > 1)
    require(isReadEnd(t.head._1, t.tail.head._1))

    val a = causingActionOfReturn(t)

    if a.isEmpty then List() // Only possible if no write has linearized yet
    else {
      val b = causingRegWriteOfAction(a)
      if b.isEmpty then List() // Should not be possible
      else causingWriteOfRegWrite(b)
    }
  }

  // Helper functions on traces of Tromp's algorithm

  def generate_intermediate_event(t: List[(State, Action)]): IntermediateEvent = {
    require(isTrompTrace(t))
    require(t.length > 1)

    val current  = t.head._1
    val previous = t.tail.head._1

    (current, previous) match
      case _ if isWriteStart(current, previous)    => BeginWrite(!current.REG.value)
      case _ if isWriteEnd(current, previous)      => EndWrite(current.REG.value)
      case _ if isReadStart(current, previous)     => BeginRead()
      case _ if isReturnAux(current, previous)     => ReturnAux(current.readerState.ret.get)
      case _ if isReturnVal(current, previous)     => ReturnVal(current.readerState.ret.get)
      case _ if isSetAux(current, previous)        => SetAux(current.readerState.aux)
      case _ if isSetVal(current, previous)        => SetVal(current.readerState.value)
      case _ if isRegWriteStart(current, previous) => BeginWriteREG(!current.REG.value)
      case _ if isRegWriteEnd(current, previous)   => BeginWriteREG(current.REG.value)
      case _                                       => OtherEvent()
  }

  def traceToIntermediateExecution(t: List[(State, Action)]): List[IntermediateEvent] = {
    require(isTrompTrace(t))
    decreases(t)

    t match {
      case Cons((current, _), Cons((previous, a), _)) => {
        val rec = traceToIntermediateExecution(t.tail)

        val current_event = generate_intermediate_event(t)

        current_event match {
          case OtherEvent()  => rec
          case current_event => Cons(current_event, rec)
        }
      }
      case _ => {
        List()
      }
    }
  }
}
