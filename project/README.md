# Formal Verification of an Atomic Bit

## Overview

This repository contains the Scala implementation and formal verification of an atomic bit, following Tromp's algorithm. Our project aims to demonstrate the application of formal verification to concurrent computing, specifically in the context of constructing and validating an atomic bit using Stainless, a formal verification tool built for Scala.

## Authors

- Basil Contovounesios
- Auguste Poiroux
- Manuel Vidigueira

## Project Structure

- `src`: Contains the source code of the Finite State Machine (FSM) implementation, the high-level specification of the atomic bit, and all the necessary lemmas to prove the correctness of the implementation.
- For more detailed information about the theory behind the project, please refer to the [project report](report.pdf) and the [project presentation](slides.pdf).

## Installation and Setup

### Requirements

- Scala version: 3.2.0
- Stainless: 0.9.8.1

Here is the complete output of `stainless --version`:

```bash
[  Info  ] Stainless verification tool (https://github.com/epfl-lara/stainless)
[  Info  ] Version: 0.9.8.1
[  Info  ] Built at: 2023-09-21 16:19:34.140+0200
[  Info  ] Stainless Scala version: 3.2.0
[  Info  ] Bundled Scala compiler: 3.2.0
```

###  Setup Instructions

- Clone the repository: `git clone https://gitlab.epfl.ch/poiroux/formal-verification`
- Navigate to the project directory in this repository: `cd project`

## Verification

To verify the project using Stainless, make sure you are in the `project` folder of the repository. Then run the following command:

    stainless src/*.scala

**Note:** As mentioned in the project report, the verification of Tromp algorithm is not complete and is still relying on some unproven intermediate lemmas. Here is an extract of the output of the verification of the current state of the project:

```bash
[  Info  ] ║ total: 856  valid: 830  (556 from cache, 15 trivial) invalid: 0    unknown: 26   time:  182,53
```
